FROM php:7.4-apache
RUN mv $PHP_INI_DIR/php.ini-development $PHP_INI_DIR/php.ini
RUN pecl install psr-1.1.0 \
	&& docker-php-ext-enable psr
RUN pecl install phalcon-4.1.2 \
	&& docker-php-ext-enable phalcon
RUN docker-php-ext-install pdo_mysql
RUN pecl install xdebug-3.1.1 \
	&& docker-php-ext-enable xdebug \
	&& echo "xdebug.mode=debug" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
	&& echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
	&& echo "xdebug.client_port=9003" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini \
	&& echo "xdebug.idekey=PHPSTORM" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
RUN a2enmod rewrite \
	&& service apache2 restart
